app.factory('responseObserver', function responseObserver($q, $window, $location, toaster) {
    return {
        'responseError': function (errorResponse) {
            switch (errorResponse.status) {
                // case 400:
                //     toaster.pop('error', "error", "400:Bad request");

                    // break;
                case 401:
                    toaster.pop('error', "error", "401: Unauthorized");

                    break;
                // case 403:
                //     toaster.pop('error', "error", "403: User doesn't exist");

                    break;
                case 409:
                    toaster.pop('error', "error", "409: User already exist");
                    break;
                case 404:
                    toaster.pop('error', "error", "401: Unauthorized");
                    
                    break;
                case 500:
                toaster.pop('error', "error", "500: Internal server error");
                    
                    break;
                case 502:
                toaster.pop('error', "error", "502: Service temporarily unavailable");
                    
                    break;
            }
            return $q.reject(errorResponse);
        }
    };
});