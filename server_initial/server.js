/******************************** Node Packages *************************************/
import express from 'express';
import osprey from 'osprey';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import session from 'express-session';
import config from 'config';
import path from 'path';
import csurf from 'csurf';
import methodOverride from 'method-override';

/******************************** Local imports *************************************/
import { db } from './models/mongodb';
import { passport } from './services/passport';
import { logger } from './utils/logger';

/********************************* Variable listing *********************************/
var envConfig;
const env = process.env.NODE_ENV || 'development';
const commonConfig = config.get('commonConfig');

/**************** Env config ****************/
if(env === 'production') {
    envConfig = config.get('env.production');
} else {
    envConfig = config.get('env.development');
}

var ramlFile = path.resolve('./raml', 'api-v1.raml');
const ramlConfig = {
  "server": {
    "notFoundHandler": false
  },
  "disableErrorInterception": true
};

startServer();

function startServer() {
  osprey.loadFile(ramlFile, ramlConfig)
    .then(function (middleware) {

      // ================================================ Instantiate the app.
      const app = express();
      
      if (env === 'production') {
        logger.info('=========  Running in PRODUCTION mode =========')
        let accessLogStream = fs.createWriteStream(envConfig.accessLogFile, {
          flags: 'a'
        });
        let errorLogStream = fs.createWriteStream(envConfig.errorLogFile, {
          flags: 'a'
        });
        morgan.format(
          'custom1',
          ':date[iso] :response-time ms :remote-addr - :remote-user ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"');
        let theAccessLog = morgan('custom1', {
          skip: function (req, res) {
            return res.statusCode >= 400
          },
          stream: accessLogStream
        });
        let theErrorLog = morgan('custom1', {
          skip: function (req, res) {
            return res.statusCode < 400
          },
          stream: errorLogStream
        });
        app.use(theAccessLog);
        app.use(theErrorLog);
      } else {
        logger.info(' Running in DEVELOPMENT mode ');
        app.use(morgan('dev'));
      }

      app.disable('x-powered-by');
      
      app.use((req, res, next) => {
        if (env === 'production') {
          res.header("Access-Control-Allow-Origin", "http://104.236.138.79"); // Change for Prod
        } else {
          res.header("Access-Control-Allow-Origin", "http://localhost:8080");
        }
        res.header("Access-Control-Allow-Credentials", "true");
        res.header("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With, x-xsrf-token");
        res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
        if (req.method === 'OPTIONS') {
          return res.status(200).send();
        } else {
          next();
        }
      });
      app.use(cookieParser());
      // app.use(csurf({
      //   cookie: true
      // }))
      app.use(bodyParser.json());
      app.use(bodyParser.urlencoded({
        extended: true
      }));
      
      if (env === 'production') {
        app.use(session({
          secret: 'Qb8nH9vecCmzKwdtnTmDHhemTNz63R', // Change the secret
          resave: false,
          saveUninitialized: true,
          cookie: {
            maxAge: 3600000,
            secure: true
          },
          name: 'webwallet.sid'
        }));

      } else {
        app.use(session({
          secret: 'Qb8nH9vecCmzKwdtnTmDHhemTNz63R', // change the secret
          resave: false,
          saveUninitialized: true,
          cookie: {
            maxAge: 3600000,
            secure: false
          },
          name: 'webwallet.sid'
        }));
      }

      app.use(passport.initialize());
      app.use(passport.session());
      
      // Express only serves static assets in production
      app.use(express.static('../client'));

      app.use(methodOverride('X-HTTP-Method')) // Microsoft
      app.use(methodOverride('X-HTTP-Method-Override')) // Google/GData
      app.use(methodOverride('X-Method-Override')) // IBM

      app.use(function (req, res, next) {
        req.mongodb = db;
        next();
      })
      
      var router = osprey.Router();
      
      // set up all the routes found in the routes directory
      var routes = require('./routes')(router);
    
      // Mount the RAML middleware at our base /api/v1
      app.use(commonConfig.apiMountPoint, middleware, routes);
      app.use(customNotFoundHandler);
      app.use(errorChecker);
      
      var server = app.listen((process.env.PORT || envConfig.port), () => {
        logger.info(`${commonConfig.serverName} at http://localhost:${server.address().port}${commonConfig.apiMountPoint}`);
      });

      app.get('*',function(req,res){
        res.sendFile( __dirname + '../client/index.html');
      });
      
    })
    .catch(function (e) {
      logger.error("Error: %s", e);
      process.exit(1)
    });
}

const errorChecker = (err, req, res, next) => {
  if (err) {
    logger.error("Error checker server", err);
    let _err;
    if (env === 'development') {
      _err = "ERROR: Validation failed. ";
    } else {
      _err = 'Internal Error'
    }
    res.status(400);
    return res.json({
      status: false,
      "message": _err
    });
  } else {
    return next();
  }
}

const customNotFoundHandler = (req, res, next) => {
  if (req.resourcePath) {
    return next()
  } else {
    res.status(404);
    return res.json({
      status: false,
      "message": `The path ${req.path} is not found`
    });
  }
}