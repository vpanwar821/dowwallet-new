import winston from 'winston';
import fs from 'fs';
var logDirectory = './logs';

winston.setLevels(winston.config.npm.levels);
winston.addColors(winston.config.npm.colors);

if (!fs.existsSync(logDirectory)) {
  // Create the directory if it does not exist
  fs.mkdirSync(logDirectory);
}

let logger = new (winston.Logger)({
  transports: [
    new winston.transports.Console({
      level: 'debug',
      handleExceptions: true,
      json: false,
      colorize: true
    }),
    new winston.transports.File({
      name: 'error-file',
      level: 'error',
      timestamp: function () { return Date() },
      filename: logDirectory + '/error.log',
      handleExceptions: true,
      json: true
    }),
    new winston.transports.File({
      name: 'info-file',
      level: 'info',
      timestamp: function () { return Date() },
      filename: logDirectory + '/wallet.log',
      handleExceptions: true,
      json: true
    })
  ],
  exitOnError: false
});
export {logger};