// Eventually we have to set up all the API endpoints this way
// The benefit of using Osprey is that it does all the  schema validation for you
// on the way in (and eventually on the way out)
//

// authentication
import {
  passport,
  requireAuth,
  requireSignin,
  encryptPassword,
  tokenForUser,
  getRandom, 
  sendHtmlMail,
  encryptNewPassword
} from '../services/passport';
import ethers from 'ethers';
import {
  encrypt 
} from '../helpers/encryption';
import request from 'request';
import {
  db
} from '../models/mongodb';
import bcrypt from 'bcrypt-nodejs';
import path from 'path';
import moment from 'moment';


function addUserToDatabase(req, res, next) {
  const users = req.mongodb.get('users');  
  let addrid, userid;
  var wallet = new ethers.Wallet.createRandom();
  var OTPCode;
  if (!req.body.email || !req.body.password || !req.body.firstName || !req.body.lastName || !req.body.country || !req.body.telephone) {
    return res.json({
      "status": "error",
      code: 400,
      "message": "Missing parameters"
    });
  }
  users.findOne({email: req.body.email.toLowerCase()})
  .then((user) => {
      if (user) {
        return res.status(405).json({
          "status": "error",
          code: 405,
          "message": "User already exists"
        });
      }
     

     OTPCode = getRandom(100000, 999999);
      
      users.insert({

                             email: req.body.email.trim().toLowerCase(),
                             ethereumAddress: wallet.address,
                             ethereumPrivKey: encrypt(wallet.privateKey, req.body.password),
                             password: req.body.encryptedPassword,
                             firstName: req.body.firstName,
                             lastName: req.body.lastName,
                             telephone: req.body.telephone,
                             country: req.body.country,
                             emailOTP: OTPCode,
                             emailOtpCreatedAt: Date.now(),
                             isAuthenticated:false,
                             is2FAOn:false
      
                              });

      
       var onlypath = path.join(__dirname,'../views/emails/verifyEmailOtp.hbs');
       sendHtmlMail(
         {
             otp: OTPCode,
             firstName : req.body.firstName,
             lastName : req.body.lastName
           },
   req.body.email, 'Verify Email - Dowcoin', onlypath);
       return res.status(201).json({
         "status":"success",
         "code" : 201,
         "message" :"Verification otp sent to mail",
          "data" : { 
           "username": req.body.email,
           
         }
       });
 })
    

     
 .catch((err) => {
    return res.status(500).json({
      "status": "Failure",
      "code": 500,
      "message": "Problem saving user at the last step",
    })
  })
}   

function verifyEmailOtp(req,res,next){
  const users = req.mongodb.get('users');
  var userDetails;
  users.findOne({
    email: req.body.email.toLowerCase()
  })
  .then((user)=>{
    userDetails = user;
    if(!user){
      return res.status(403).json({
        "status": "error",
        code: 403,
        "message": "User doesnot exists"
      });
    }
    else {
      if(user.emailOTP === parseInt(req.body.OTP)){
        console.log("emailOTP");
        var currentDate = moment(new Date());
        var otpDate = moment(user.emailOtpCreatedAt);
        var newDiff = currentDate.diff(otpDate, 'seconds');
        if(newDiff > 480){
          console.log("timediff");
          return res.status(405).json({
            "status": "error",
            code: 405,
            "message": "Otp has been expired"
          }); 
        }
        else{
          var myquery = {email:user.email};
          var newvalues = {$set:{isAuthenticated:true}};
          return users.update(myquery,newvalues,function(err,res){
            if(err) throw err;
            console.log("success");
          })
        }
      }
      else{
        return res.status(405).json({
          "status": "error",
          code: 405,
          "message": "Invalid Otp"
        }); 
      }
    }
  })
  .then(() => {
    return tokenForUser({
      email: req.body.email.trim().toLowerCase()
    })
  })
  .then((token) => {
  res.cookie('jwt', token )
  return res.json({
    "status": "success",
    "code": 201,
    "message": "Created",
    "data": {
      "username": req.body.email,
      "userId" : userDetails._id
    }
  })
  })
  .catch((err) => {
    return res.status(500).json({
      "status": "Failure",
      "code": 500,
      "message": "Problem saving user at the last step",
    })
  })

}

function verifyEmailForgotPassword(req,res,next){
  const users = req.mongodb.get('users');
  users.findOne({
    email: req.body.email.toLowerCase()
  })
  .then((user)=>{

    if(!user){
      return res.status(403).json({
        "status": "error",
        code: 403,
        "message": "User doesnot exists"
      });
    }
    else {

      return  res.status(200).json({
        "status": "success",
        "code": 200,
        "message": "Ok"
       
      });
    
    }
  })
  
  .catch((err) => {
    return res.status(500).json({
      "status": "Failure",
      "code": 500,
      "message": "Problem saving user at the last step",
    })
  })

}

function verifyOtpForgotPassword(req,res,next){
  const users = req.mongodb.get('users');
  users.findOne({
    email: req.body.email.toLowerCase()
  })
  .then((user)=>{


      if(user.emailOTP === parseInt(req.body.OTP)){
        console.log("emailOTP");
        var currentDate = moment(new Date());
        var otpDate = moment(user.emailOtpCreatedAt);
        var newDiff = currentDate.diff(otpDate, 'seconds');
        if(newDiff > 480){
          console.log("timediff");
          return res.status(405).json({
            "status": "error",
            code: 405,
            "message": "Otp has been expired"
          }); 
        }
        else{
          return  res.status(200).json({
            "status": "success",
            "code": 200,
            "message": "Ok"
           
          });
        }
      }
      else{
        return res.status(405).json({
          "status": "error",
          code: 405,
          "message": "Invalid Otp"
        }); 
      }
    
  })
  
  .catch((err) => {
    return res.status(500).json({
      "status": "Failure",
      "code": 500,
      "message": "Problem saving user at the last step",
    })
  })

}


function verifyNewPassword(req,res,next){
  const users = req.mongodb.get('users');
  users.findOne({
    email: req.body.email.toLowerCase()
  })
  .then((user)=>{


    if(req.body.newPassword === req.body.newPasswordOne)
    {
    var myquery = {email:user.email};
    
    
    
    var newvalues = {$set:{password: req.body.encryptedPassword}};
    users.update(myquery,newvalues,function(err,res){
      if(err) throw err;
      console.log("success");
    })
    return  res.status(200).json({
      "status": "success",
      "code": 200,
      "message": "Ok"
     
    });
  }
  else{
    return  res.status(405).json({
      "status": "error",
      "code": 405,
      "message": "Password doesnot match"
     
    });

  }
    
  })
  
  .catch((err) => {
    return res.status(500).json({
      "status": "Failure",
      "code": 500,
      "message": "Problem saving user at the last step",
    })
  })

}



function resendEmailOtp(req,res,next){
  const users = req.mongodb.get('users');
  users.findOne({
    email: req.body.email.toLowerCase()
  })
  .then((user)=>{

    if(!user){
      return res.status(403).json({
        "status": "error",
        code: 403,
        "message": "User doesnot exists"
      });
    }
    else {
      var currentDate = moment(new Date());
      var otpDate = moment(user.emailOtpCreatedAt);

      var newDiff = currentDate.diff(otpDate, 'seconds');
      if(newDiff > 480)
      {
        var OTPCode = getRandom(100000, 999999);
        var myquery = {email:user.email};
        var newvalues = {$set:{emailOTP:OTPCode,emailOtpCreatedAt:Date.now()}};
        users.update(myquery,newvalues,function(err,res){
          if(err) throw err;
          console.log("success");
        })
        var onlypath = path.join(__dirname,'../views/emails/verifyEmailOtp.hbs');
        sendHtmlMail(
          {
              otp: OTPCode,
              firstName : user.firstName,
              lastName : user.lastName
            },
    user.email, 'Verify Email - Dowcoin', onlypath);
        return res.status(201).json({
          "status":"success",
          "code" : 201,
          "message" :"Verification otp sent to mail",
           "data" : { 
            "username": req.body.email,
            
          }
        });
      }
      else{

          var OTPcode = user.emailOTP; 
          var onlypath = path.join(__dirname,'../views/emails/verifyEmailOtp.hbs');
                  sendHtmlMail(
                    {
                        otp: user.emailOTP,
                        firstName : user.firstName,
                        lastName : user.lastName
                      },
              user.email, 'Verify Email - Dowcoin', onlypath);
                  return res.status(201).json({
                    "status":"success",
                    "code" : 201,
                    "message" :"Verification otp sent to mail",
                     "data" : { 
                      "username": req.body.email,
                      
                    }
                  });
          
      }
    }
  })
  
  .catch((err) => {
    return res.status(500).json({
      "status": "Failure",
      "code": 500,
      "message": "Problem in sending verification otp",
    })
  })

}


function forgotPassword (req,res,next){
  const users = req.mongodb.get('users');
  users.findOne({
    email: req.body.email.toLowerCase()
  })
  .then((user)=>{

    if(!user){
      return res.status(405).json({
        "status": "error",
        code: 405,
        "message": "User doesnot exists"
      });
    }
    else {
          if(!user.isAuthenticated)
          {
            return res.status(405).json({
              "status": "error",
              code: 405,
              "message": "User is not authentic"
            });
          }
    
          else{


            var OTPCode = getRandom(100000, 999999);
            var myquery = {email:user.email};
            var newvalues = {$set:{emailOTP:OTPCode,emailOtpCreatedAt:Date.now()}};
            users.update(myquery,newvalues,function(err,res){
              if(err) throw err;
              console.log("success");
            })
           
            var onlypath = path.join(__dirname,'../views/emails/verifyEmailOtp.hbs');
                  sendHtmlMail(
                    {
                        otp: OTPCode,
                        firstName: user.firstName,
                        lastName : user.lastName
                      },
              user.email, 'Verify Email - Dowcoin', onlypath);
                  return res.status(203).json({
                    "status":"success",
                    "code" : 201,
                    "message" :"Verification otp sent to mail",
                     "data" : { 
                      "username": req.body.email,
                      
                    }
                  });
            
              // else success, return the user record
             
              
          


           
       }
    }
  })
  
  .catch((err) => {
    return res.status(500).json({
      "status": "Failure",
      "code": 500,
      "message": "Problem in sending verification otp",
    })
  })

}

function resetPassword (req,res,next){
  const users = req.mongodb.get('users');
  users.findOne({
    email: req.body.email.toLowerCase()
  })
  .then((user)=>{

    if(!user){
      return res.status(403).json({
        "status": "error",
        code: 403,
        "message": "User doesnot exists"
      });
    }
    else {
          if(!user.isAuthenticated)
          {
            return res.status(405).json({
              "status": "error",
              code: 405,
              "message": "User is not authentic"
            });
          }
    
          else{
            if(req.body.newPassword === req.body.newPasswordOne)
            {
            var myquery = {email:user.email};
            var newvalues = {$set:{password: req.body.encryptedPassword}};
            users.update(myquery,newvalues,function(err,res){
              if(err) throw err;
              console.log("success");
            })
         
          return  res.status(200).json({
            "status": "success",
            "code": 200,
            "message": "Password changed.",
           
          });
        }
        else{
          return  res.status(405).json({
            "status": "error",
            "code": 405,
            "message": "Password doesnot match"
           
          });

        }
         }
    }
  })
  
  .catch((err) => {
    return res.status(500).json({
      "status": "Failure",
      "code": 500,
      "message": "Problem in sending verification otp",
    })
  })

}


function doSignin(req, res, next) {
  const users = req.mongodb.get('users');
  users.findOne({
    email: req.body.email.toLowerCase()
  })
  .then((user)=>{
    if(!user){
      return res.status(405).json({
        "status": "error",
        code: 405,
        "message": "User doesnot exists"
      });
    }
    else{

      

      if(!user.isAuthenticated){
        var OTPCode = getRandom(100000, 999999);
        var myquery = {email:user.email};
        var newvalues = {$set:{emailOTP:OTPCode,emailOtpCreatedAt:Date.now()}};
        users.update(myquery,newvalues,function(err,res){
          if(err) throw err;
          console.log("success");
        })
            
        var onlypath = path.join(__dirname,'../views/emails/verifyEmailOtp.hbs');                        
        sendHtmlMail(
          {
              otp: OTPCode,
              firstName : user.firstName,
              lastName : user.lastName
            },
    user.email, 'Verify Email - Dowcoin', onlypath);
        return res.status(201).json({
          "status": "success",
          code: 201,
          "message": "Email verification is incomplete.Verification otp has sent to your mail.",
          "data": {
            "username": req.body.email,
            "is2FAOn": true,
            "isAuthenticated":false
         }
        });
      
    }
    else if(user.is2FAOn){
      var OTPCode = getRandom(100000, 999999);
      var myquery = {email:user.email};
      var newvalues = {$set:{emailOTP:OTPCode,emailOtpCreatedAt:Date.now()}};
      users.update(myquery,newvalues,function(err,response){
        if(err) throw err;
        console.log("success");
      })
          
      var onlypath = path.join(__dirname,'../views/emails/verifyEmailOtp.hbs');
      sendHtmlMail(
        {
            otp: OTPCode,
            firstName : user.firstName,
            lastName : user.lastName
          },
     user.email, 'Verify Email - Dowcoin', onlypath);
     return res.status(201).json({
      "status":"success",
      "code" : 201,
      "message" :"Verification otp sent to mail for 2FA.",
       "data" : { 
        "username": req.body.email,
        "userId" : user._id,
        "is2FAOn": true
      }
    });

    }

    // g-recaptcha-response is the key that browser will generate upon form submit.
  // if its blank or null means user has not selected the captcha, so return the error.
  // if(req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
  //   return res.status(403).json({
  //     "status": "error",
  //     code: 403,
  //     "message": "Please select captcha"
  //   });
  // }
  // // Put your secret key here.
  // var secretKey = "6Lfo3zsUAAAAAAGJjQqL0_CaZutkt1DSYzlsfgbt";
  // // req.connection.remoteAddress will provide IP address of connected user.
  // var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body['g-recaptcha-response'] + "&remoteip=" + req.connection.remoteAddress;
  // // Hitting GET request to the URL, Google will respond with success or error scenario.
  // request(verificationUrl,function(error,response,body) {
  //   body = JSON.parse(body);
  //   // Success will be true or false depending upon captcha validation.
  //   if(body.success !== undefined && !body.success) {
  //     return  res.status(500).json({
  //       "status": "Failure",
  //       "code": 500,
  //       "message": "Failed captcha verification"
       
  //     });
  //   }
    return tokenForUser(req.user);
    // res.status(201).json({
    //   "status": "success",
    //   "code": 201,
    //   "message": "Success",
     
    // });
  // });
  }
  })
  // .then(()=>{
  //   return tokenForUser(req.user)
  // })
  .then((token) => {
    res.cookie('jwt', token, { sameSite: true}) // add secure for HTTPS
    return res.status(200).json({
      "status": "success",
      "code": 200,
      "message": "ok",
      "data": {
        "username": req.body.email,
        "userId" : req.user._id,
        "is2FAOn": false,
        "isAuthenticated":true
     }
    });
  })
}

function doLogout(req, res, next) {
  res.clearCookie('jwt');
  // res.clearCookie('_csrf');
  return res.status(200).json({
    "status": "success",
    "code": 200,
    "message": "Ok",
  });
}

function twoFactorAuthentication(req, res, next){
  const users = req.mongodb.get('users');

  users.findOne({
    email: req.body.email.toLowerCase()
  })
  .then((user)=>{

    if(!user){
      return res.status(403).json({
        "status": "error",
        code: 403,
        "message": "User doesnot exists"
      });
    }
    else {
      if(req.body.is2FA === true){
      var myquery = {email:user.email};
      var newvalues = {$set:{is2FAOn:true}};
      users.update(myquery,newvalues,function(err,response){
        if(err) throw err;
        return res.status(200).json({
          "status": "success",
          "code": 200,
          "message": "Two factor authentication is on"
        });
      })
    }
    else{
      var myquery = {email:user.email};
      var newvalues = {$set:{is2FAOn:false}};
      users.update(myquery,newvalues,function(err,response){
        if(err) throw err;
        return res.status(200).json({
          "status": "success",
          "code": 200,
          "message": "Two factor authentication is off"
        });
      })
    }
    }
  })
}

function getUserDetails(req, res, next){
  const users = req.mongodb.get('users');

  users.findOne({
    email: req.params.email.toLowerCase()
  })
  .then((user)=>{

    if(!user){
      return res.status(403).json({
        "status": "error",
        code: 403,
        "message": "User doesnot exists"
      });
    }
      if(!user.isAuthenticated){
        return res.status(405).json({
          "status": "error",
          "code": 405,
          "message": "Unauthenticated user"
        })
      }
      var userDetails = {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        country: user.country,
        telephone: user.telephone,
        is2FAOn: user.is2FAOn
      }
      return res.status(200).json({
          "status": "success",
          "code": 200,
          "message": "user details fetched",
          "data": userDetails
        });
  })
}

/*

function getCaptcha(req,res){
  // g-recaptcha-response is the key that browser will generate upon form submit.
  // if its blank or null means user has not selected the captcha, so return the error.
  if(req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
    return res.json({"responseCode" : 1,"responseDesc" : "Please select captcha"});
  }
  // Put your secret key here.
  var secretKey = "--paste your secret key here--";
  // req.connection.remoteAddress will provide IP address of connected user.
  var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body['g-recaptcha-response'] + "&remoteip=" + req.connection.remoteAddress;
  // Hitting GET request to the URL, Google will respond with success or error scenario.
  request(verificationUrl,function(error,response,body) {
    body = JSON.parse(body);
    // Success will be true or false depending upon captcha validation.
    if(body.success !== undefined && !body.success) {
      return res.json({"responseCode" : 1,"responseDesc" : "Failed captcha verification"});
    }
    res.json({"responseCode" : 0,"responseDesc" : "Sucess"});
  });
});
*/



module.exports = function (router) {

  router.get('/auth/token', (req, res) => {
    const referer = req.headers.referer
    console.log("Auth token ", referer);
    let refererToTest;
    if(process.env.NODE_ENV === 'production') {
      refererToTest = /104.65.56.35/
    } else {
      refererToTest = /localhost:8080/
      
    }
    if (!refererToTest.test(referer)) {
      res.status(401).send();
    }
    const token = req.csrfToken();
    return res.append('X-XSRF', req.csrfToken()).send({
      csrf: token
    })
  });

  router.post('/auth/register',
    (req, res, next) => {
      next();
    },
    encryptPassword,
    addUserToDatabase
    
  );

  router.post('/auth/login',
    (req, res, next) => {
      next();
    },
    requireSignin,
    doSignin);

  router.post('/auth/logout',
    (req, res, next) => {
      next();
    },
    doLogout);

    router.post('/auth/verify',
    (req, res, next) => {
      next();
    },
    verifyEmailOtp);

    router.post('/auth/resend',
    (req, res, next) => {
      next();
    },
    resendEmailOtp);

    router.post('/auth/forgot',
    (req, res, next) => {
      next();
    },
    forgotPassword);

    router.post('/auth/verifyEmail',
    (req, res, next) => {
      next();
    },
    verifyEmailForgotPassword);

    router.post('/auth/verifyForgot',
    (req, res, next) => {
      next();
    },
    verifyOtpForgotPassword);

    router.post('/auth/verifyNewPassword',
    (req, res, next) => {
      next();
    },
    encryptNewPassword,
    verifyNewPassword);

    router.post('/auth/reset',
    (req, res, next) => {
      next();
    },
    encryptNewPassword,
    resetPassword);


    router.post('/auth/twoFactor',
    (req, res, next) => {
      next();
    },
    twoFactorAuthentication);

    router.get('/auth/userDetails/{email}',
    (req, res, next) => {
      next();
    },
    getUserDetails);

    /*router.post('/auth/captcha',
    (req, res, next) => {
      next();
    },
    getCaptcha);*/
    

    

}