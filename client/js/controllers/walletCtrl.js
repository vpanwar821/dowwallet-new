app.controller('walletCtrl', function (Wallet, $scope, $window,toaster) {
	$scope.formData = {};
	$scope.exportPrivKeyError = false;
	$scope.importPrivKeyError = false;
	$scope.importUtcError = false;

	$scope.exportPrivKey = function () {

		$scope.exportPrivKeyError = false;
		var data = {
			email: $window.localStorage.getItem('email'),
			password: $scope.formData.exportKeyPassword
		}
		console.log("Export Private Key request", data);

		if (data.email !== undefined && data.password !== undefined) {
			Wallet.exportPrivKey(data).then(function (result) {
				console.log("Export private key response", result);
				if (result.data.status) {
					console.log("Export key success message", result.data.message);
					$scope.privateKey = result.data.data;
					$scope.formData.exportKeyPassword = undefined;
					toaster.pop('success', "Success", result.data.message);
				} else {
					console.log("Export key error message", result.data.message);
					$scope.exportPrivKeyError = true;
					$scope.exportPrivKeyErrorMessage = result.data.message;
					toaster.pop('error', "error", result.data.message);
				}
			},function(error){
				
				toaster.pop('error', "error", error.data.message);
	
			});
		} else {
			console.log("Invalid fields");
			$scope.exportPrivKeyError = true;
			$scope.exportPrivKeyErrorMessage = "Invalid fields";
		}

	}

	$scope.importPrivKey = function () {

		$scope.importPrivKeyError = false;
		var data = {
			email: $window.localStorage.getItem('email'),
			password: $scope.formData.importKeyPassword,
			privKey: $scope.formData.privKey
		}
		console.log("Import Private Key request", data);

		if (data.email !== undefined && data.password !== undefined && data.privKey !== undefined) {
			Wallet.importPrivKey(data).then(function (result) {
				console.log("Import private key response", result);
				if (result.data.status) {
					console.log("Import key success message", result.data.message);
					$scope.addressThroughKey = result.data.data;
					$scope.formData.importKeyPassword = undefined;
					$scope.formData.privKey = undefined;
					toaster.pop('success', "Success", result.data.message);

				} else {
					console.log("Import key error message", result.data.message);
					$scope.importPrivKeyError = true;
					$scope.importPrivKeyErrorMessage = result.data.message;
					// toaster.pop('error', "error", result.data.message);
				}
			},function(error){
				
				toaster.pop('error', "error", error.data.message);
	
			});
		} else {
			console.log("Invalid fields");
			$scope.importPrivKeyError = true;
			$scope.importPrivKeyErrorMessage = "Invalid fields";
		}

	}

	$scope.showContent = function($fileContent){
        $scope.content = $fileContent;
    };

	$scope.importUtc = function () {

		$scope.importUtcError = false;
		var data = {
			email: $window.localStorage.getItem('email'),
			password: $scope.formData.importUtcPassword,
			utcFile: JSON.parse($scope.content)
		}
		console.log("Import UTC request", data);

		if (data.email !== undefined && data.password !== undefined && data.utcFile !== undefined) {
			Wallet.importUtc(data).then(function (result) {
				console.log("Import UTC response", result);
				if (result.data.status) {
					console.log("Import utc success message", result.data.message);
					$scope.addressThroughUtc = result.data.data;
					$scope.formData.importUtcPassword= undefined;
					$scope.content= undefined;
					var file = angular.element("input[type='file']");
					file.val(null);
					file.parent().find('label').find('strong')[0].innerHTML = "Choose a file";

					toaster.pop('success', "Success", result.data.message);

				} else {
					console.log("Import utc error message", result.data.message);
					$scope.importUtcError = true;
					$scope.importUtcErrorMessage = result.data.message;
					toaster.pop('error', "error", result.data.message);
				}
			},function(error){
				
				toaster.pop('error', "error", error.data.message);
	
			});
		} else {
			console.log("Invalid fields");
			$scope.importUtcError = true;
			$scope.importUtcErrorMessage = "Invalid fields";
		}
	}

})