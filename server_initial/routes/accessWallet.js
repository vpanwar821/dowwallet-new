import ethers from 'ethers';
import bcrypt from 'bcrypt-nodejs';
import {
  web3
} from '../services/ethereumService';
import {
  requireAuth
} from '../services/passport';
import {
  db
} from '../models/mongodb';
import { encrypt, decrypt } from '../helpers/encryption';
import { logger } from '../utils/logger';

function check(req, res, next) {

  console.log("Accounts", web3.eth.accounts);
  res.status(200).send({
    status: true,
    data: web3.eth.accounts
  });

}

function exportPrivKey(req, res, next) {

  console.log("Export request ", req.body);
  const users = req.mongodb.get('users');

  users.findOne({ email: req.body.email.toLowerCase() })
    .then((user) => {
      if (!user) {
        return res.status(403).json({
          "status": "error",
          code: 403,
          "message": "User doesnot exists"
        });
      } else {
        const privKey = decrypt(user.ethereumPrivKey, req.body.password);
        res.status(200).send({
          "status": "success",
          code:200,
          "message": "Success",
          data: privKey.toString()
        });
      }
    })
    .catch((err) => {
      console.log("error while exporting key", err);
      return res.status(403).json({
        "status": "Failure",
        "code": 403,
        "message": "Incorrect password",
      })
    })
}

function importPrivKey(req, res, next) {

  console.log("Import request ", req.body);
  const users = req.mongodb.get('users');
  const password = req.body.password;
  const email = req.body.email;
  var wallet = new ethers.Wallet(req.body.privKey);

  users.findOne({
    email: email.toLowerCase()
  })
    .then((user) => {

      if (!user) {
        return res.status(403).json({
          "status": "error",
          code: 403,
          "message": "User doesnot exists"
        });
      }
      else {
        // if (!user.isAuthenticated) {
        //   return res.status(407).json({
        //     "status": "error",
        //     code: 407,
        //     "message": "User is not authentic"
        //   });
        // }

        // else {
          const passwordFromDb = user.password;
          bcrypt.compare(password, passwordFromDb, (err, isMatch) => {
            if (err) {
              done(err);
              return null;
            }
            if (!isMatch) {
              done(null, false, { message: 'Password not matched ' + email });
              return null;
            }
            var myquery = { email: email };
            var newvalues = { $push: { additionalEthereumAddresses: wallet.address, additionalPrivKey: encrypt(wallet.privateKey, password) } };
            users.update(myquery, newvalues, function (err, response) {
              if (err) throw err;
              res.status(200).send({
                "status": "success",
                code:200,
                "message":"Wallet imported successfully",
                data: wallet.address
              });
            });
          });
        // }
      }
    })
    .catch((err) => {
      console.log("error ",err);
      return res.status(510).json({
        "status": "Failure",
        "code": 510,
        "message": "Problem in storing existing wallet details",
      })
    })

}


function accessThroughUtc(req, res, next) {

  console.log("Utc access request ", req.body);
  const users = req.mongodb.get('users');
  var json = JSON.stringify(req.body.utcFile);
  var password = req.body.password;
  var email = req.body.email;
  ethers.Wallet.fromEncryptedWallet(json, password)
    .then(function (wallet) {
      console.log("Address: " + wallet.address);
      var myquery = { email: email };
      var newvalues = { $push: { additionalEthereumAddresses: wallet.address, additionalPrivKey: encrypt(wallet.privateKey, password) } };
      users.update(myquery, newvalues, function (err, response) {
        if (err) throw err;
        res.status(200).send({
          "status": "success",
          code:200,
          message: "Wallet imported successfully",
          data: wallet.address
        });
      });
    })
    .catch(function (error) {
      logger.error(error);
      res.status(403).send({
        "status":"error",
        code:403,
        message: "Invalid password or error while updating existing wallet"
      });
    })
}

function accessThroughMnemonic(req, res, next) {

  console.log("Mnemonic access request ", req.body);

  ethers.Wallet.fromMnemonic(req.body.mnemonic)
    .then(function (wallet) {
      console.log("Address: " + wallet.address);
      res.status(200).send({
        status: true,
        data: wallet.address
      });
    })
    .catch(function (error) {
      logger.error(error);
      res.status(403).send({
        status: false,
        message: "Invalid mnemonic"
      });
    })
}


module.exports = function (router) {

  router.get('/access/check',
    (req, res, next) => {
      next();
    },
    check
  );

  router.post('/access/exportKey',
    (req, res, next) => {
      next();
    },
    // requireAuth,
    exportPrivKey
  );

  router.post('/access/importKey',
    (req, res, next) => {
      next();
    },
    // requireAuth,
    importPrivKey
  );

  router.post('/access/throughUtc',
    (req, res, next) => {
      next();
    },
    // requireAuth,
    accessThroughUtc
  );

  router.post('/access/throughMnemonic',
    (req, res, next) => {
      next();
    },
    // requireAuth,
    accessThroughMnemonic
  );


}