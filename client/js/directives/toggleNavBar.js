app.directive('toogleNavBar', function(){
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
    
                function toggleNavBar () {
                    var sidebar = angular.element(document.getElementById('sidebar'));
                    var overlay = angular.element(document.getElementsByClassName('overlay'));
                    if( sidebar.hasClass('active')){
                        sidebar.removeClass('active');
                        overlay.removeClass('block');
                        overlay.addClass('hide');
                    }else{
                        sidebar.addClass('active');
                        var a = angular.element(document.querySelectorAll('a[aria-expanded=true]'))
                        a.attr('aria-expanded', 'false');
                        overlay.removeClass('hide');
                        overlay.addClass('block');
                    }
                }
                
                element.on('click', toggleNavBar);
            }
        };
    });