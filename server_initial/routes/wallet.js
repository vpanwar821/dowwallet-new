import ethers from 'ethers';
import {
    web3,
    balanceOfToken
} from '../services/ethereumService';
import {
    requireAuth
} from '../services/passport';
import { logger } from '../utils/logger';

const providers = ethers.providers;
const network = providers.networks.ropsten;
const etherscanProvider = new providers.EtherscanProvider(network);

function getBalance(req, res, next) {

    console.log("Balance request ", req.params);

    etherscanProvider.getBalance(req.params.address).then(function (balance) {

        var etherString = ethers.utils.formatEther(balance);
        console.log("Balance: " + etherString);
        res.status(200).send({
            status: "success",
            data: etherString
        });
    });
}

function balanceOf(req, res, next) {
    if (!req.params.address) {
      return res.json({
        "status": "Failure",
        "code": 400,
        "message": "Missing parameters",
      })
    }
    return res.status(201).json({
      "status": "success",
      "code": 201,
      "message": "balanceOf",
      "data": {
        "balance": balanceOfToken(req.params.address),
      }
    });
  };


module.exports = function (router) {

    router.get('/balance/{address}',
        (req, res, next) => {
            next();
        },
        // requireAuth,
        getBalance
    );
    router.get('/balanceOfToken/{address}',
    (req, res, next) => {
      next();
    },
    // requireAuth,
    balanceOf
  );
}