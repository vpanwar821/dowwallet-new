import fs from 'fs';
import monk from 'monk';
import { logger } from '../utils/logger';

let db;
var env = process.env.NODE_ENV || 'development';

if (env === 'production') {
  let cert = fs.readFileSync('../cert/server.pem');
  let pemKey = fs.readFileSync('../cert/tokenExchange.pem');
  const connectionOptions = {
    ssl: true,
    sslCA: [cert], // CA
    sslKey: pemKey,
    sslCert: pemKey, // client key (self signed)
    sslPass: 'DOWWallet', // password
    autoReconnect: true
  }
  db = monk('mongodb://walletServer:DOWWallet@10.134.1.46:28018/tokenExchange', connectionOptions); // change admin and password 
  logger.info("Database connected for production env");
} else {
  db = monk('mongodb://localhost:27017/dowwallet',function(err) {
    if (err) 
      {
        logger.error(err.message);
        process.exit(1);
      }      
    else
      logger.info("Database connected for development env db_name: dowwallet");
  });  
}
export {db};