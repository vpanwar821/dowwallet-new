app.controller('transactionCtrl', function (Transaction, $scope, $window, toaster) {
    
        $scope.itemsPerPage = 10;
    
        $scope.getTransactionHistory = function () {
            toaster.pop('success', "Processing...", "Getting transaction history.");
            var address = $window.localStorage.getItem('ethereumAddress');
            console.log("Get Transaction History request", address);
    
            if (address !== undefined) {
                Transaction.getTransactionHistory(address).then(function (result) {
                    console.log("Get Transaction History response", result);
                    $scope.transactionList = result.data.transactions;
                    $scope.displayedCollection = result.data.transaction;
                    console.log("transactionList", $scope.transactionList);
                });
            } else {
                console.log("Missing address");
                toaster.pop('error', "error", "Something went wrong");
            }
        }
    
        $scope.getTokenTransactionHistory = function () {
            toaster.pop('success', "Processing...", "Getting transaction history.");
            var address = $window.localStorage.getItem('ethereumAddress');
            console.log("Get Token Transaction History request", address);
    
            if (address !== undefined) {
                Transaction.getTokenTransactionHistory(address).then(function (result) {
                    console.log("Get Token Transaction History response", result);
                    if (result.data.status === "success") {
                        console.log("Get Token Transaction History success message", result.data.message);
                        $scope.transactionList = result.data.data;
                    } else {
                        console.log("Get Token Transaction History error message", result.data.message);
                        toaster.pop('error', "error", result.data.message);
                    }
                });
            } else {
                console.log("Missing address");
                toaster.pop('error', "error", "Something went wrong");
            }
        }
    
    })