app.controller('homeCtrl', function (User, $scope, $state, $window,toaster) {
	$scope.formData = {};
	$scope.signupError = false;
	$scope.loginError = false;
	$scope.verifyEmailError = false;
	$scope.forgotPasswordError = false;
	$scope.resetPasswordError = false;

	/* Signup api */
	$scope.signup = function () {

		$scope.signupError = false;
		
		var data = {
			firstName: $scope.formData.firstName,
			lastName: $scope.formData.lastName,
			telephone: $scope.formData.telephone,
			country: $scope.formData.country,
			email: $scope.formData.signupEmail,
			password: $scope.formData.signupPassword
		};
		console.log("Signup request data", data);

		if (data.firstName !== undefined && data.lastName !== undefined && data.telephone !== undefined && data.country !== undefined && data.email !== undefined && data.password !== undefined) {
			if ($scope.formData.signupPassword === $scope.formData.confirmPassword) {
				User.signup(data).then(function (result) {
					console.log("Signup response", result);
					if (result.data.status === "success") {
						console.log("Signup success message", result.data.message);
						$window.localStorage.email = result.data.data.username;
						$state.go('verifyOtp');
					} 
					else {
						console.log("Signup error message", result.data.message);
						$scope.signupError = true;
												
					// 	toaster.pop('error', "error", "user already exist");

					}
				}, function(error){
					console.log("Signup error message", error);
					// $scope.signupError = true;
					
					// $scope.signupErrorMessage = result.data.message;
					toaster.pop('error', "error", error.data.message);
				});
			}
			else {
				console.log("Password not matched");
				// $scope.signupError = false;
				
				
				toaster.pop('error', "error", "Password not matched");
			}
		} else {
			if(data.password === undefined)
			{
			$scope.signupError = true;
			
			toaster.pop('error', "error", "Invalid fields.");
			console.log("Data undefined");
			}
			else
			{
				$scope.signupError = false;

				toaster.pop('error', "error", "Invalid fields.");
			}
		}
	}

	$scope.login = function () {
		$scope.loginError = false;
		var data = {
			email: $scope.formData.loginEmail,
			password: $scope.formData.loginPassword
		};
		console.log("Login request data", data);
		if (data.email !== undefined && data.password !== undefined) {
			User.login(data).then(function (result) {
				console.log("Login response", result);
				if (result.data.status === "success") {
					console.log("login success message", result.data.message);
					$window.localStorage.email = result.data.data.username;
					$window.localStorage.userId = result.data.data.userId;
					if(result.data.data.is2FAOn||!result.data.data.isAuthenticated){
						$state.go('verifyOtp');
						// toaster.pop('success', "Success", result.data.message);
					} else {
						console.log("redirect to dashboard");
						$state.go('dashboard', { 'userId' : result.data.data.userId});
					}
				} else {
					console.log("login error message", result.data.message);
					$scope.loginError = true;
					$scope.loginErrorMessage = result.data.message;
					toaster.pop('error', "error", result.data.message);
				}
			});
		} else {
			console.log("Invalid fields");
			$scope.loginError = true;
			$scope.loginErrorMessage = "Invalid fields";
		}
	}

	$scope.verifyEmail = function () {

		$scope.verifyEmailError = false;
		var data = {
			email: $window.localStorage.getItem('email'),
			OTP: $scope.formData.verifyOTP
		};
		console.log("Verify email request data", data);
		if(data.email !== undefined && data.OTP !== undefined){
		User.verifyEmail(data).then(function (result) {
			console.log("Verify email response", result);
			if (result.data.status === "success") {
				console.log("Verify email success message", result.data.message);
				$window.localStorage.email = result.data.data.username;
				$window.localStorage.userId = result.data.data.userId;
				if($window.localStorage.getItem('forgotPassword')){
					$window.localStorage.removeItem('forgotPassword');
					$state.go('enterNewPassword');
				} else {
					console.log("redirect to dashboard");
					$state.go('dashboard', { 'userId' : result.data.data.userId});
				}
			} else {
				console.log("Verify email error message", result.data.message);
				$scope.verifyEmailError = true;
				$scope.verifyEmailErrorMessage = result.data.message;
			}
		},function(error){
			console.log("Forgot password error message", error.data.message);
			toaster.pop('error', "error", error.data.message);

		});
	} else {
		console.log("Invalid fields");
		$scope.verifyEmailError = true;
		$scope.verifyEmailErrorMessage = "Invalid fields";
	}
	}

	$scope.token = function () {
		
				User.token().then(function (result) {
					console.log("Token response", result);
				});
			}

	$scope.resendOtp = function () {

		var data = {
			email: $window.localStorage.getItem('email')
		}
		console.log("Resend Otp request", data);

		User.resendOtp(data).then(function (result) {
			console.log("resend OTP response", result);
			if (result.data.status === "success") {
				console.log("resend otp success message", result.data.message);
				toaster.pop('success', "Success", result.data.message);

			} else {
				console.log("resend otp error message", result.data.message);
			}
		});

	}

	$scope.forgotPassword = function () {

		$scope.forgotPasswordError = false;
		var data = {
			email: $scope.formData.forgotPasswordEmail
		}
		console.log("Forgot password request", data);
		if(data.email !== undefined){
		User.forgotPassword(data).then(function (result) {
			console.log("Forgot password response", result);
			if (result.data.status === "success") {
				console.log("Forgot password success message", result.data.message);
				$window.localStorage.forgotPassword = true;
				$window.localStorage.email = $scope.formData.forgotPasswordEmail;
				$state.go('verifyOtp');
			} 
			else {
				console.log("Forgot password error message", result.data.message);
				toaster.pop('error', "error", result.data.message);
			}
		},function(error){
			console.log("Forgot password error message", error.data.message);
			toaster.pop('error', "error", error.data.message);

		});
	} else {
		console.log("Invalid email");
		$scope.forgotPasswordError = true;
	}
	}

	$scope.resetPassword = function () {
		
				$scope.resetPasswordError = false;
				
				var data = {
					email: $window.localStorage.getItem('email'),
					newPassword: $scope.formData.newPassword,
					newPasswordOne: $scope.formData.newPasswordOne
				}
				console.log("Reset Password data", data);
		
				if (data.email !== undefined && data.newPassword !== undefined && data.newPassword !== undefined) {
					if (data.newPassword === data.newPasswordOne) {
						User.resetPassword(data).then(function (result) {
							console.log("Reset password response", result);
							if(result.data.status === "success"){
								$state.go('home');
								toaster.pop('success', "Success", result.data.message);
							} else {
								$scope.resetPasswordError = true;
								$scope.resetPasswordErrorMessage = result.data.data.message;
								toaster.pop('error', "error", result.data.message);
							}
						},function(error){
							$scope.tokenButton = false;
							toaster.pop('error', "error", error.data.message);
				
						});
					} else {
						console.log("Password not matched");
						$scope.resetPasswordError = false;
						
						$scope.resetPasswordErrorMessage = "Password not matched";
						toaster.pop('error', "error", "Password not matched");
					}
				} else {
					
					console.log("Missing parameters");
					$scope.resetPasswordError = true;
					
					$scope.resetPasswordErrorMessage = "Invalid fields";
					toaster.pop('error', "error","Invalid fields");
					
						
					
				}
		
			}

			$scope.open = function () {
				
					var modalInstance = $modal.open({
					  templateUrl: 'myModalContent.html',
					  controller: 'dashboardCtrl'
					
					  
					});
				}

})