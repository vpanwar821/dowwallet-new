app.directive('uploadFile', function(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            function fileUploaded (event) {
                var fileName = '';

                fileName = event.target.value.split( '\\' ).pop();

                element.parent().find('label').find('strong')[0].innerHTML = fileName;
                
            }
            
            element.on('change', fileUploaded);
        }
    };
});