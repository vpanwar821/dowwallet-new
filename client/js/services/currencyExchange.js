app.factory('Exchange',function ($http) {
    exchangeFactory={};

    exchangeFactory.rates = function () {
        return $http.get('api/v1/rates');
    }
    
    return exchangeFactory;
    
});