app.factory('Transaction',function ($http) {
    txFactory={};

    txFactory.getTransactionHistory = function (ethereumAddress) {
        return $http.get('api/v1/transactions/'+ ethereumAddress + '/history');
    }

    txFactory.getTokenTransactionHistory = function (ethereumAddress) {
        return $http.get('api/v1/transactions/'+ ethereumAddress + '/tokenHistory');
    }

    return txFactory;
    
});