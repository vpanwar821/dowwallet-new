import ethers from 'ethers';
import {
    waitUntilMined,
    web3,
    getReceipt
} from '../services/ethereumService';
// addresses
import {
    requireAuth
} from '../services/passport.js';

import config from 'config';
import BigNumber from 'bignumber.js';
BigNumber.config({ ERRORS: false });
import rp from 'request-promise';

const commonConfig = config.get('commonConfig');
const apiKey = commonConfig.etherscan_api_key;
const end_of_url = `&startblock=0&endblock=99999999&sort=asc&apikey=${apiKey}`;
const etherscanApiUrls = {
    'dev': 'http://ropsten.etherscan.io/api?module=account&action=txlist&address=',
    'prod': 'http://api.etherscan.io/api?module=account&action=txlist&address='
}
let start_of_url;
if (process.env.NODE_ENV === 'production') {
    start_of_url = etherscanApiUrls['prod'];
} else {
    start_of_url = etherscanApiUrls['dev'];
}

function getConfirmationsByHash(req, res, next) {
    if (!req.body.ethereum_address) {
        res.status(400).json({
            "status": "Bad Request",
            "code": 400,
            "message": "Missing parameters",
        })
    }
    const ethAddress = req.body.ethereum_address.toLowerCase(); // maybe save ethereum address in user
    let ethTransaction;
    let apiUrl = start_of_url + ethAddress + end_of_url;

    rp(apiUrl)
        .then(json => {
            const transactionHistory = JSON.parse(json).result;
            if (transactionHistory.length === 0) return res.status(204).send();
            for (let i = 0; i < transactionHistory.length; i++) {
                if (transactionHistory[i].hash == req.params.txHash) {
                    ethTransaction = transactionHistory[i];
                    return res.status(200).json({
                        "transaction": ethTransaction
                    });
                }
            };
        }).catch(e => {
            return res.status(500).send({
                "status": "Failure",
                "code": 500,
                "message": "Error: Confirmation by Hash",
            });
        });
}

function getTransactionHistory(req, res, next) {
    const ethAddress = req.params.ethereum_address.toLowerCase();
    // ETHERSCAN
    let apiUrl = start_of_url + ethAddress + end_of_url;

    rp(apiUrl)
        .then(json => {
            const transactionHistory = JSON.parse(json).result;
            console.log(transactionHistory);
            return res.status(200).json({
                "transactions": transactionHistory
            });
        }).catch(e => {
            return res.status(500).send({
                "status": "Failure",
                "code": 500,
                "message": "Error: Transaction History",
            });
        });
}

function getTokenTransactionHistory(req, res, next) {
    let ethAddress = req.params.ethereum_address.toLowerCase();

    ethAddress = ethAddress.slice(2)
    const topic1 = '0x000000000000000000000000' + ethAddress;
    const etherscanApiUrls = {
        'dev': `https://ropsten.etherscan.io/api?module=logs&action=getLogs&fromBlock=0&toBlock=latest&address=${commonConfig.token_contract_address}&topic0=${commonConfig.topic}&topic0_1_opr=and&topic1=${topic1}&topic1_2_opr=or&topic2=${topic1}&apikey=${apiKey}`,
        'prod': `https://api.etherscan.io/api?module=logs&action=getLogs&fromBlock=0&toBlock=latest&address=${commonConfig.token_contract_address}&topic0=${commonConfig.topic}&topic0_1_opr=and&topic1=${topic1}&topic1_2_opr=or&topic2=${topic1}&apikey=${apiKey}`
    }
    let apiUrl;
    if (process.env.NODE_ENV === 'production') {
        apiUrl = etherscanApiUrls['prod'];
    } else {
        apiUrl = etherscanApiUrls['dev'];
    }
    rp(apiUrl)
        .then(json => {
            const transactionHistory = JSON.parse(json).result;
            console.log(transactionHistory);
            return res.status(200).json({
                "transactions": transactionHistory
            });
        }).catch(e => {
            return res.status(500).send({
                "status": "Failure",
                "code": 500,
                "message": "Error: Token Transaction History",
            });
        });
}

function getTransactionFromEtherScan(req, res, next) {
    const ethAddress = req.body.ethereum_address.toLowerCase();
    // ETHERSCAN
    let apiUrl = start_of_url + ethAddress + end_of_url;
    rp(apiUrl)
        .then(json => {
            const transactionHistory = JSON.parse(json).result;
            return res.status(200).json({
                "transactions": +ethTransaction.confirmations
            });
        }).catch(e => {
            return res.status(500).send({
                "status": "Failure",
                "code": 500,
                "message": "Error: Transaction from Etherscan",
            });
        });
}


function watchConfirmation(req, res, next) {
    const ethDepositAddress = req.body.ethereum_address.toLowerCase();
    let ethTransaction;
    // ETHERSCAN
    let apiUrl = start_of_url + ethAddress + end_of_url;

    rp(apiUrl).then(response => {
        const transactionHistory = JSON.parse(response).result;

        for (let i = 0; i < transactionHistory.length; i++) {
            if (transactionHistory[i].hash = req.params.txHash && transactionHistory[i].confirmations > 0) {
                ethTransaction = transactionHistory[i];
                return res.status(200).send({
                    transaction: ethTransaction
                });
            }
        };
    }).catch(e => {
        return res.status(500).send({
            "status": "Failure",
            "code": 500,
            "message": "Error: Watch for confirmation",
        });
    });
}

function waitForMining(req, res, next) {
    const txn = waitUntilMined(req.params.txHash)
    if (txn) {
        return res.status(200).json({
            transaction: txn
        })
    } else {
        return res.status(201).json({transaction: null})
    }
}

function getTransactionReceipt(req, res, next) {
    return res.status(200).json({
        "status": "Success",
        "code": 200,
        "message": getReceipt(req.params.txHash),
    })
}

module.exports = function (router) {

    router.post('/transactions/{txHash}',
        (req, res, next) => {
            next();
        },
        requireAuth,
        getConfirmationsByHash
    );

    router.get('/transactions/{txHash}',
        (req, res, next) => {
            next();
        },
        requireAuth,
        getTransactionReceipt
    );

    router.get('/transactions/{txHash}/mine',
        (req, res, next) => {
            next();
        },
        requireAuth,
        waitForMining
    );

    router.post('/transactions',
        (req, res, next) => {
            next();
        },
        requireAuth,
        getConfirmationsByHash
    );

    router.get('/transactions/{ethereum_address}/history',
        (req, res, next) => {
            next();
        },
        // requireAuth,
        getTransactionHistory
    );

    router.get('/transactions/{ethereum_address}/tokenHistory',
        (req, res, next) => {
            next();
        },
        // requireAuth,
        getTokenTransactionHistory
    );
}