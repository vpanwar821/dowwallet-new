app.factory('User',function ($http) {
    userFactory={};

    userFactory.token = function () {
        return $http.get('api/v1/auth/token');
    }
    
    userFactory.signup = function (userData) {
        return $http.post('api/v1/auth/register', userData);
    }

    userFactory.login = function (userData) {
        return $http.post('api/v1/auth/login', userData);
    }

    userFactory.verifyEmail = function (userData) {
        return $http.post('api/v1/auth/verify', userData);
    }

    userFactory.logout = function () {
        return $http.post('api/v1/auth/logout', {
            headers: {'Content-Type': 'application/json'}
        });
    }

    userFactory.resendOtp = function (userData) {
        return $http.post('api/v1/auth/resend', userData);
    }

    userFactory.forgotPassword = function (userData) {
        return $http.post('api/v1/auth/forgot', userData);
    }

    userFactory.resetPassword = function (userData) {
        return $http.post('api/v1/auth/reset', userData);
    }

    userFactory.twoFactorAuthentication = function (userData) {
        return $http.post('api/v1/auth/twoFactor', userData);
    }

    userFactory.getUserDetails = function (email) {
        return $http.get('api/v1/auth/userDetails/' + email);
    }

    return userFactory;
    
});