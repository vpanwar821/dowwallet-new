app.controller('dashboardCtrl', function (User, Address, Exchange, $scope, $state, $window,$modal, toaster) {
	$scope.formData = {};
	$scope.transferError = false;
	$scope.addressList = [];

	

	$scope.logout = function () {

		User.logout().then(function (result) {
			console.log("Logout response", result);
			$window.localStorage.removeItem('email');
			$window.localStorage.removeItem('userId');
			$window.localStorage.removeItem('ethereumAddress');
			toaster.pop('success', "success", "Logout successfully");
			$state.go('home');
		});
	}

	$scope.rates = function () {
		
		Exchange.rates().then(function (result) {
					console.log("Rates response", result);
					$scope.btcRate = result.data.BTC;
					$scope.ethRate = result.data.ETH;
				});
	}

	$scope.getContributionAddress = function () {

		Address.contributionAddress().then(function (result) {
			console.log("Contribution Address response", result);
			if (result.data.status === "success") {
				console.log("Export key success message", result.data.message);
				$scope.contributionAddress = result.data.data;
			} else {
				console.log("Contribution address error message", result.data.message);
				toaster.pop('error', "error", result.data.message);
			}
		});
	}

	$scope.getAllAddresses = function () {

		var email = $window.localStorage.getItem('email');
		console.log("Get all addresses request", email);

		if (email !== undefined) {
			Address.getAllAddresses(email).then(function (result) {
				console.log("Get all addresses response", result);
				if (result.data.status === "success") {
					console.log("Get all addresses success message", result.data.message);
					$window.localStorage.ethereumAddress = result.data.data.ethereumAddress;
					$scope.ethereumAddress = result.data.data.ethereumAddress;
					// $scope.additionalEthereumAddresses = result.data.data.additionalEthereumAddresses;
					// $scope.addressList = result.data.data.additionalEthereumAddresses;
					$scope.addressList.push({
						name: result.data.data.ethereumAddress});
					console.log($scope.addressList);
				} else {
					console.log("Get all addresses error message", result.data.message);
					toaster.pop('error', "error", result.data.message);
				}
			});
		} else {
			console.log("Missing email");
			toaster.pop('error', "error", "Missing email");
		}
	}

	$scope.transferToken = function () {

		$scope.tokenButton = true;
		$scope.transferError = false;	
		toaster.pop('success', "Processing..", "Transaction is in process.");
		var data = {
			email: $window.localStorage.getItem('email'),
			password: $scope.formData.transferPassword,
			value: $scope.formData.value,
			to: $scope.formData.to
		}
		console.log("Transfer token request", data);

		if (data.email !== undefined && data.password !== undefined && data.value !== undefined && data.to !== undefined) {
			
			Address.transferToken(data).then(function (result) {
				console.log("Transfer token response", result);
				
				if (result.data.status === "success") {
					console.log("Transfer token success message", result.data.message);
					$scope.transactionToken=result.data.data.hash;
					$scope.formData.currency = undefined;
					$scope.formData.to = undefined;
					$scope.formData.value = undefined;
					$scope.formData.transferPassword = undefined;
					$scope.tokenButton = false;
					 toaster.pop('success', "Success", result.data.message);
					
				} else {
					console.log("Transfer token error message", result.data.message);
					$scope.transferError = true;
					$scope.tokenButton = false;
					$scope.transferErrorMessage = result.data.message;
				}
			},function(error){
				$scope.tokenButton = false;
				toaster.pop('error', "error", error.data.message);
	
			});
		} else {
			console.log("Invalid fields");
			$scope.transferError = true;
			$scope.tokenButton = false;
			$scope.transferErrorMessage = "Invalid fields";
		}
	}

	$scope.transferEther = function () {

		$scope.tokenButton = true;
		$scope.transferError = false;
		toaster.pop('success', "Processing..", "Transaction is in process.");
		var data = {
			email: $window.localStorage.getItem('email'),
			password: $scope.formData.transferPassword,
			value: $scope.formData.value,
			to: $scope.formData.to
		}
		console.log("Transfer ether request", data);

		if (data.email !== undefined && data.password !== undefined && data.value !== undefined && data.to !== undefined) {
			
			Address.transferEther(data).then(function (result) {
				console.log("Transfer ether response", result);
				
				if (result.data.status === "success") {
					console.log("Transfer ether success message", result.data.message);
					$scope.transactionToken=result.data.data.hash;
					$scope.formData.currency = undefined;
					$scope.formData.to = undefined;
					$scope.formData.value = undefined;
					$scope.formData.transferPassword = undefined;
					$scope.tokenButton = false;
					 toaster.pop('success', "Success", result.data.message);
					// $state.transitionTo($state.current, null, { reload: true, notify: true });
					
				} else {
					console.log("Transfer ether error message", result.data.message);
					$scope.transferError = true;
					$scope.tokenButton = false;
					$scope.transferErrorMessage = result.data.message;
				}
			},function(error){
				$scope.tokenButton = false;
				toaster.pop('error', "error", error.data.message);
	
			});
		} else {
			console.log("Invalid fields");
			$scope.transferError = true;
			$scope.tokenButton = false;
			$scope.transferErrorMessage = "Invalid fields";
		}
	}

	$scope.getUserDetails = function () {

		var email = $window.localStorage.getItem('email');
		console.log("get user details request", email);

		if (email !== undefined) {
			User.getUserDetails(email).then(function (result) {
				console.log("Get user details response", result);
				if (result.data.status === "success") {
					console.log("get user details success response", result.data.message);
					$scope.user = result.data.data;
				} else {
					console.log("get user details error response", result.data.message);
					toaster.pop('error', "error", result.data.message);
				}
			})
		}

	}

	$scope.twoFactorAuthentication = function () {

		var data;
		if ($scope.user.is2FAOn) {
			data = {
				email: $window.localStorage.getItem('email'),
				is2FA: true
			}
		} else {
			data = {
				email: $window.localStorage.getItem('email'),
				is2FA: false
			}
		}
		console.log("Two factor authentication", data);

		if (data.email !== undefined && data.is2FA !== undefined) {
			User.twoFactorAuthentication(data).then(function (result) {
				console.log("Two factor authentication response", result);
				if (result.data.status === "success") {
					console.log("two factor authentication success message", result.data.message);
					toaster.pop('success', "Success", result.data.message);
				} else {
					console.log("two factor authentication error message", result.data.message);
					toaster.pop('error', "error", result.data.message);
				}
			});
		} else {
			console.log("Missing parameters");
			toaster.pop('error', "error", "Something went wrong");
		}
	}

	$scope.getWalletBalance = function () {

		var address = $window.localStorage.getItem('ethereumAddress');
		console.log("get wallet balance request", address);

		if (address !== undefined) {
			Address.getBalance(address).then(function (result) {
				console.log("get wallet balance", result);
				if (result.data.status === "success") {
					console.log("get wallet balance success message", result.data.message);
					$scope.walletBalance = result.data.data;
				} else {
					console.log("get wallet balance error message", result.data.message);
					toaster.pop('error', "error", result.data.message);
				}
			});
		} else {
			console.log("missing parameter address");
			toaster.pop('error', "error", "Something went wrong");
		}
	}

	$scope.getTokenBalance = function () {
		
				var address = $window.localStorage.getItem('ethereumAddress');
				console.log("get token balance request", address);
		
				if (address !== undefined) {
					Address.getTokenBalance(address).then(function (result) {
						console.log("get token balance", result);
						if (result.data.status === "success") {
							console.log("get token balance success message", result.data.message);
							$scope.tokenBalance = parseFloat(result.data.data.balance)/Math.pow(10, 18);
						} else {
							console.log("get token balance error message", result.data.message);
							toaster.pop('error', "error", result.data.message);
						}
					});
				} else {
					console.log("missing parameter address");
					toaster.pop('error', "error", "Something went wrong");
				}
			}

			$scope.resetPassword = function () {
				
						$scope.resetPasswordError = false;
						
						var data = {
							email: $window.localStorage.getItem('email'),
							newPassword: $scope.formData.newPassword,
							newPasswordOne: $scope.formData.newPasswordOne
						}
						console.log("Reset Password data", data);
				
						if (data.email !== undefined && data.newPassword !== undefined && data.newPassword !== undefined) {
							if (data.newPassword === data.newPasswordOne) {
								User.resetPassword(data).then(function (result) {
									console.log("Reset password response", result);
									if(result.data.status === "success"){
										$scope.formData.newPassword=undefined;
										$scope.formData.newPasswordOne=undefined;
										$state.go('dashboard.settings');
										
										toaster.pop('success', "Success", result.data.message);
									} else {
										$scope.resetPasswordError = true;
										$scope.resetPasswordErrorMessage = result.data.data.message;
										toaster.pop('error', "error", result.data.message);
									}
								},function(error){
									$scope.tokenButton = false;
									toaster.pop('error', "error", error.data.message);
						
								});
							} else {
								console.log("Password not matched");
								$scope.resetPasswordError = false;
								
								$scope.resetPasswordErrorMessage = "Password not matched";
								toaster.pop('error', "error", "Password not matched");
							}
						} else {
							
							console.log("Missing parameters");
							$scope.resetPasswordError = true;
							
							$scope.resetPasswordErrorMessage = "Invalid fields";
							toaster.pop('error', "error","Invalid fields");
							
								
							
						}
				
					}

					$scope.open = function () {
						
							var modalInstance = $modal.open({
							  templateUrl: 'myModalContent.html',
							  controller: 'dashboardCtrl'
							
							  
							});
						}

						
						

})