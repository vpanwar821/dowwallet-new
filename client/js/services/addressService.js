app.factory('Address',function ($http) {
    addressFactory={};

    addressFactory.getAllAddresses = function (email) {
        return $http.get('api/v1/addresses/'+ email);
    }

    addressFactory.getBalance = function (address) {
        return $http.get('api/v1/balance/'+ address);
    }

    addressFactory.getTokenBalance = function (address) {
        return $http.get('api/v1/balanceOfToken/'+ address);
    }

    addressFactory.contributionAddress = function () {
        return $http.get('api/v1/addresses/contribute');
    }
    
    addressFactory.transferToken = function (addressData) {
        return $http.post('api/v1/transfer/token', addressData);
    }

    addressFactory.transferEther = function (addressData) {
        return $http.post('api/v1/transfer/ether', addressData);
    }

    return addressFactory;
    
});