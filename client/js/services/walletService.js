app.factory('Wallet',function ($http) {
    walletFactory={};

    walletFactory.exportPrivKey = function (walletData) {
        return $http.post('api/v1/access/exportKey', walletData);
    }
    
    walletFactory.importPrivKey = function (walletData) {
        return $http.post('api/v1/access/importKey', walletData);
    }

    walletFactory.importUtc = function (walletData) {
        return $http.post('api/v1/access/throughUtc', walletData);
    }

    return walletFactory;
    
});