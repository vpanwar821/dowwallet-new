// app.js
var app = angular.module('app', [
    'ui.router', 
    'ui.bootstrap', 
    'monospaced.qrcode', 
    'toaster',
    'smart-table']);

app.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

    $httpProvider.interceptors.push('responseObserver');
    
    $urlRouterProvider.otherwise('/home');
    $urlRouterProvider.when("/dashboard/:userId", "/dashboard/:userId/transaction");
    
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'templates/home.html',
			controller: 'homeCtrl'
        })
        .state('register', {
            url: '/register',
            templateUrl: 'templates/register.html',
			controller: 'homeCtrl'
        })
        .state('forgotPassword', {
            url: '/forgotPassword',
            templateUrl: 'templates/forgotPassword.html',
			controller: 'homeCtrl'
        })
        .state('verifyOtp', {
            url: '/verifyOtp',
            templateUrl: 'templates/verifyOtp.html',
			controller: 'homeCtrl'
        })
        .state('enterNewPassword', {
            url: '/enterNewPassword',
            templateUrl: 'templates/enterNewPassword.html',
			controller: 'homeCtrl'
        })
        .state('dashboard', {
            url: '/dashboard/:userId',
            templateUrl: 'templates/dashboard/dashboard.html',
			controller: 'dashboardCtrl'
        })
        .state('dashboard.transaction', {
            url: '/transaction',
            templateUrl: 'templates/dashboard/transaction.html',
			controller: 'dashboardCtrl'
        })
        .state('dashboard.address', {
            url: '/address',
            templateUrl: 'templates/dashboard/address.html',
			controller: 'dashboardCtrl'
        })
        .state('dashboard.balance', {
            url: '/balance',
            templateUrl: 'templates/dashboard/balance.html',
			controller: 'dashboardCtrl'
        })
        .state('dashboard.transfer', {
            url: '/transfer',
            templateUrl: 'templates/dashboard/transfer.html',
			controller: 'dashboardCtrl'
        })
        .state('dashboard.settings', {
            url: '/settings',
            templateUrl: 'templates/dashboard/settings.html',
			controller: 'dashboardCtrl'
        })
        .state('dashboard.importExport', {
            url: '/importExportKey',
            templateUrl: 'templates/dashboard/importExportKey.html',
			controller: 'walletCtrl'
        });
        
        
});

  


app.run(['$window', '$rootScope', '$document', function($window, $rootScope, $document) {
	// $rootScope.isLogin = $window.localStorage.getItem('isLogin');
    // $rootScope.providerSet = $window.localStorage.getItem('providerSet');
	// $rootScope.userId = $window.localStorage.getItem('userId');
    // $rootScope.headerName = $window.localStorage.getItem('userName');

    // $rootScope.$on('$stateChangeSuccess',function(){
    // $("html, body").animate({ scrollTop: 0 }, 200);
    // });

   
		 

          
}]);

