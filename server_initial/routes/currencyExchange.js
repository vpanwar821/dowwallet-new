import rp from 'request-promise';

function getExchangeRate(req, res, next) {

    let apiUrl = 'https://api.coinmarketcap.com/v1/ticker/';
    const result = {};

    rp(apiUrl)
        .then(json => {
            const rates = JSON.parse(json);
            var rateCount = rates.length;
            for (let i = 0; i < rateCount; i++) {
                    switch(rates[i].symbol) {
                        case 'BTC':
                            result.BTC = rates[i].price_usd;
                            break;
                        case 'ETH':
                            result.ETH = rates[i].price_usd;
                            break;
                    }
                };
            return res.status(200).json(result);
        }).catch(e => {
            return res.status(400).send({
                error: 'Problem getting rates from the API' + e
            });
        });
}

module.exports = function (router) {

    router.get('/rates',
        (req, res, next) => {
            next();
        },
        getExchangeRate
    );
}