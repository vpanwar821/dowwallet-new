/*****************************Node modules ***************************/
import BigNumber from 'bignumber.js';
BigNumber.config({ ERRORS: false });
import Web3 from 'web3';
import EthereumTx from 'ethereumjs-tx';
import config from 'config';

/***************************Import local modules **************************/
import promisifyWeb3 from '../helpers/promisifyWeb3';
import { decrypt } from '../helpers/encryption';

const env = process.env.NODE_ENV || 'development';
export let web3;

if (env === 'production') {
  web3 = promisifyWeb3(new Web3(new Web3.providers.HttpProvider(`https://mainnet.infura.io/mROzXUBaQKkqSyXQFXLq`)));
} else {
  web3 = promisifyWeb3(new Web3(new Web3.providers.HttpProvider(`https://ropsten.infura.io/eenkErjk3rtwXMAOCZjb`)));
}

const contractAbi = [
    {
      "constant": true,
      "inputs": [],
      "name": "name",
      "outputs": [
        {
          "name": "",
          "type": "string"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_spender",
          "type": "address"
        },
        {
          "name": "_value",
          "type": "uint256"
        }
      ],
      "name": "approve",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "totalAllocatedTokens",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "totalSupply",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "foundersAllocation",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_from",
          "type": "address"
        },
        {
          "name": "_to",
          "type": "address"
        },
        {
          "name": "_value",
          "type": "uint256"
        }
      ],
      "name": "transferFrom",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "founderMultiSigAddress",
      "outputs": [
        {
          "name": "",
          "type": "address"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "decimals",
      "outputs": [
        {
          "name": "",
          "type": "uint8"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "initialSupply",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "devAllocation",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "crowdFundAddress",
      "outputs": [
        {
          "name": "",
          "type": "address"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_newFounderMultiSigAddress",
          "type": "address"
        }
      ],
      "name": "changeFounderMultiSigAddress",
      "outputs": [],
      "payable": false,
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "_owner",
          "type": "address"
        }
      ],
      "name": "balanceOf",
      "outputs": [
        {
          "name": "balance",
          "type": "uint256"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "tokensAllocatedToCrowdFund",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "devTeamAddress",
      "outputs": [
        {
          "name": "",
          "type": "address"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "symbol",
      "outputs": [
        {
          "name": "",
          "type": "string"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_amount",
          "type": "uint256"
        }
      ],
      "name": "addToAllocation",
      "outputs": [],
      "payable": false,
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_to",
          "type": "address"
        },
        {
          "name": "_value",
          "type": "uint256"
        }
      ],
      "name": "transfer",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "_owner",
          "type": "address"
        },
        {
          "name": "_spender",
          "type": "address"
        }
      ],
      "name": "allowance",
      "outputs": [
        {
          "name": "remaining",
          "type": "uint256"
        }
      ],
      "payable": false,
      "type": "function"
    },
    {
      "inputs": [
        {
          "name": "_crowdFundAddress",
          "type": "address"
        },
        {
          "name": "_founderMultiSigAddress",
          "type": "address"
        },
        {
          "name": "_devTeamAddress",
          "type": "address"
        }
      ],
      "payable": false,
      "type": "constructor"
    },
    {
      "payable": false,
      "type": "fallback"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "name": "_blockTimeStamp",
          "type": "uint256"
        },
        {
          "indexed": true,
          "name": "_foundersWalletAddress",
          "type": "address"
        }
      ],
      "name": "ChangeFoundersWalletAddress",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "name": "from",
          "type": "address"
        },
        {
          "indexed": true,
          "name": "to",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "value",
          "type": "uint256"
        }
      ],
      "name": "Transfer",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "name": "owner",
          "type": "address"
        },
        {
          "indexed": true,
          "name": "spender",
          "type": "address"
        },
        {
          "indexed": false,
          "name": "value",
          "type": "uint256"
        }
      ],
      "name": "Approval",
      "type": "event"
    }
  ];
  
export const contractAddress = config.get('commonConfig').token_contract_address;

export const Contract = web3.eth.contract(contractAbi).at(contractAddress);

export function createRawTransaction(code, keys, to, value, password, gas) {

    if (gas === null || gas === undefined || gas < 50000) {
      gas = 150000
    }
    const privKey = decrypt(keys.privkey, password);
    keys.privkey = privKey.toString();
    const txParams = {
        nonce: web3.eth.getTransactionCount(keys.pubkey),
        gasLimit: gas,
        gasPrice: 21000000000, //21 Gwei
        value,
        to
    }
    if (code) {
      txParams.data = code;
    }
    const tx = new EthereumTx(txParams);
    tx.sign(Buffer.from(removeHexPrefix(keys.privkey), 'hex'));
    return tx.serialize();
}

export function transferToken(to, value, pubkey) {
    return Contract.transfer.getData(to, value, { from: pubkey });
}

export function transferFromToken(fromAddr, to, value, pubkey ) {
    return Contract.transferFrom.getData(fromAddr, to, value, { from: pubkey });
}

export function approveToken(spender, value, pubkey) {
    return Contract.approve.getData(spender, value, { from: pubkey });
}
export function allowanceToken(owner, spender) {
    return Contract.allowance.call(owner, spender);
}
export function balanceOfToken(address) {
    return Contract.balanceOf.call(address);
}

export function sendTransaction(serializedTx) {
  return web3.eth.sendRawTransactionPromise('0x' + serializedTx.toString('hex'));
}
    
function removeHexPrefix(str) {
  return str.replace(/^0x/, '');
}

export function getReceipt(txHash) {
  return web3.eth.getTransactionReceipt(txHash);
}

export function waitUntilMined(receipts) {
  const txn = web3.eth.getTransactionReceipt(receipts);
  if(!txn) {
    return null
  } else {
    return txn;
  }
}