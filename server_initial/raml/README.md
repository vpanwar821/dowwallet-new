# Description
This directory contains a RAML 1.0 specification, including schemas, for the W3coin Wallet API.  RAML is a specification for describing RESTful APIs.  The [RAML Specification](https://github.com/raml-org/raml-spec/blob/master/versions/raml-10/raml-10.md) is freely available.

# Rational
I wanted to try something different that would allow us to define API's quickly, along with documentation, and to make it easier to describe the collections and object schemas for each API call.

It is very common to see [Swagger](https://swagger.io) being used, but swagger is sometimes more suited to documenting existing APIs rather than creating new APIs from scratch.

RAML allows us to define the API's along with the schemas and traits, then define resources that contain those schemas and exhibit those traits.  RAML is well suited to the the [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) (Don'tRepeat Yourself) concept in software development.

From the single, well defined RAML specification of our API we can convert the API spec into both client and server side code, we can mock the API, we can test it using the mock results, and we can subsequently produce high quality documentation for others to use.  If it turns out that RAML is not the way to go, we can easily convert the RAML spec into a Swagger spec and move on.

I thought this might be a good start to learn this for use in our Etherparty API later on.

# Wikipedia
see https://en.wikipedia.org/wiki/RAML_(software)

# Online API Designer
 * [Mulesoft Designer](http://rawgit.com/mulesoft/api-designer/master/dist/index.html#/?xDisableProxy=true)
 * [Restlet Studio](http://studio.restlet.com)

# RAML 1.0 Specification
see https://github.com/raml-org/raml-spec/blob/master/versions/raml-10/raml-10.md

# Osprey code generator for RAML
https://github.com/mulesoft/osprey

# Author
Brian Onn
brian@vanbex.com
