import passport from 'passport';
import bcrypt from 'bcrypt-nodejs';
import jwt from 'jwt-simple';
import fs from 'fs';
import config from 'config';
import handlebars from 'handlebars';
import nodemailer from 'nodemailer';
import hbs from 'hbs';
import compile from 'string-template/compile';

import {
  Strategy as JwtStrategy,
  ExtractJwt
} from 'passport-jwt';

import LocalStrategy from 'passport-local';

import {db} from '../models/mongodb';

const secret = config.get('commonConfig.secrets.jwtSecret');

// Create the local login strategy
const localOptions = { usernameField: 'email' };
const localLogin = new LocalStrategy( localOptions , function(email, password, done) {
  
    const users = db.get('users');
    // Get the user record from the DB here
    users.findOne({email: email.trim().toLowerCase()}).then((user) => {
      if (!user) {
        done(null, false, { message: 'Unknown user ' + email })
      }
      const passwordFromDb = user.password;
      bcrypt.compare(password, passwordFromDb, (err, isMatch) => {
        if (err) { 
          done(err); 
          return null; 
        }
        if (!isMatch) { 
          done(null, false, { message: 'Password not matched ' + email }); 
          return null; 
        }
        // else success, return the user record
        done(null, user);
        return null;
      });
    })
  })
  


function encryptPassword (req, res, next) {
  const password = req.body.password;
  console.log("Encypt password", password);

  if (!password) {
    return res.status(422).send({ error: 'Missing password'});
  }

  // Generate a salt, then run callback.
  bcrypt.genSalt(10, (err, salt) => {
    if (err) { return err; }

    // Hash (encrypt) our password using the salt.
    bcrypt.hash(password, salt, null, (err, hash) => {
      if (err) { return err; }
      // Overwrite plain text password with encrypted password.
      req.body.encryptedPassword = hash;
      return next();
    });
  });
}

function encryptNewPassword (req, res, next) {
  const newPassword = req.body.newPassword;
  console.log("Encypt password", newPassword);

  if (!newPassword) {
    return res.status(422).send({ error: 'Missing password'});
  }

  // Generate a salt, then run callback.
  bcrypt.genSalt(10, (err, salt) => {
    if (err) { return err; }

    // Hash (encrypt) our password using the salt.
    bcrypt.hash(newPassword, salt, null, (err, hash) => {
      if (err) { return err; }
      // Overwrite plain text password with encrypted password.
      req.body.encryptedPassword = hash;
      return next();
    });
  });
}

const cookieExtractor = function(req) {
  var token = null;
  if (req && req.cookies)
  { 
      token = req.cookies['jwt'];
  }
  return token;
};

//Set up for options for JWT Strategy.
const jwtOptions = {
  jwtFromRequest: cookieExtractor,
  secretOrKey: secret
};

const jwtLogin = new JwtStrategy(jwtOptions, function(payload, done) {
  // See if the user ID in the payload exists in our DB
  // if it does, call 'done' with that
  // otherwise, call done without a user object
  const sub = payload.sub;
  const uid = payload.uid
  const users = db.get('users');
  return users.findOne({_id: uid}).then((data) => {
    if(data.email.trim().toLowerCase() !== sub) {
      done(null, false);
      return null;
    } else {
      done(null, data);
      return null; // to suppress Bluebird promise handler error
  }
  }).catch(err => {
    return done(null, false)
  })
});

const tokenForUser = function (userInfo) {
  const users = db.get('users');
  return users.findOne({email: userInfo.email.trim().toLowerCase()}).then((user) => {
    const timestamp = new Date().getTime();
    return jwt.encode(
      {
        iss: 'localhost',               //issuer TODO make this correct
        sub: user.email.trim().toLowerCase(),  // subscriber
        iat: timestamp,                 // issued at
        exp: timestamp+3600,            // expires
        uid: user._id                // user id
      }, secret);
  })
}

const getRandom = function (min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}



  
var transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
      user: 'vikaspanwar16@gmail.com',
      pass: 'Parcel8373'
  }
});
  
      // setup email data with unicode symbols
      const sendHtmlMail = function(data, to, subject, templateUrl) {

        console.log("sendhtmlmail4");
        
        readHTMLFile(templateUrl, function(err, html) {
            var template = handlebars.compile(html);
            var replacements = data;
            var htmlToSend = template(replacements);
            var msg = {
                from: 'vikaspanwar16@gmail.com',
                to: to,
                subject: subject,
                html: htmlToSend,//if you want to send html file
            };
            transporter.sendMail(msg, function(error, info){
              if(error){
                  return console.log(error);
              }
              console.log('Message sent: ' + info.response);
          });
        });
      }
  
      function readHTMLFile(path, callback) {
        fs.readFile(path,{encoding: 'utf-8'},function (err, html) {
            if (err) {
              console.log("readfileerror");
                callback(err);
            } else {
              console.log("readfilesuccess");
                callback(null, html);
            }
        });
    } 
  


passport.use('jwt', jwtLogin);
passport.use('local', localLogin);

// Passport Strategy constants
const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

export { passport, requireAuth, requireSignin, encryptPassword, tokenForUser ,getRandom, sendHtmlMail, encryptNewPassword };
