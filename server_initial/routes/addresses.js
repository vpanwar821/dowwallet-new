import config from 'config';
import {createRawTransaction, sendTransaction, transferToken } from '../services/ethereumService.js'
import BigNumber from 'bignumber.js';
const commonConfig = config.get('commonConfig');

function getContributionAddresses(req, res, next) {
  res.status(200);
  return res.json({
    "status": "success",
    "code": 200,
    "message": "Ok",
    "data": {
      "dow_ethereum_address": commonConfig.dow_ethereum_address
    }
  });
};

function getAllAddresses(req, res, next) {

  const users = req.mongodb.get('users')
  if (!req.params.email) {
    return res.status(400).json({
      "status": "Failure",
      "code": 400,
      "message": "Missing parameters"
    });
  }
  users.findOne({email: req.params.email}).then((user) => {
    if (!user) {
      return res.status(401).json({
        "status": "Failure",
        "code": 401,
        "message": "User doesnot exist"
      })
    }
    if(!user.isAuthenticated){
      return res.status(401).json({
        "status": "Failure",
        "code": 401,
        "message": "Unauthorized"
      })
    }
    var addresses = {
      ethereumAddress: user.ethereumAddress,
      additionalEthereumAddresses: user.additionalEthereumAddresses
    }
    return res.status(200).json({
      "status": "success",
      "code" : 200,
      "data": addresses
    })
  })

};

function transferNewToken(req, res, next) {
  let hash, rawTx;
  const users = req.mongodb.get('users')
  if (!req.body.password || !req.body.value || !req.body.to || req.body.value <= 0) {
    return res.json({
      "status": "error",
      "code": 400,
      "message": "Missing parameters",
    })
  }
  users.findOne({email: req.body.email}).then((user) => {
    if (!user) {
      return res.json({
        "status": "Failure",
        "code": 401,
        "message": "Unauthorized",
      })
    }
    try {
      const keys = {pubkey: user.ethereumAddress, privkey: user.ethereumPrivKey}
      const transferData = transferToken(req.body.to, new BigNumber(req.body.value).times(new BigNumber(10).pow(18)), keys.pubkey);
      const contractAddress = commonConfig.token_contract_address;
      rawTx = createRawTransaction(transferData, keys, contractAddress, 0, req.body.password, req.body.gas);

    } catch (e) {
      res.status(403);
      return res.json({
        "status": "Failure",
        "code": 403,
        "message": "Error in creating the transaction check parameters.",
      });
    }
    hash = sendTransaction(rawTx)
      .then((hash) => {
        return res.status(201).json({
          "status": "success",
          "code": 201,
          "message": "Transaction complete",
          "data": {
            "hash": hash,
          }
        });
      })
      .catch((e) => {
        res.status(500);
        return res.json({
          "status": "Failure",
          "code": 500,
          "message": "Error when sending the transaction",
        });
      })
  })
}

function transferEther(req, res, next) {
  
  let rawTx;
  const users = req.mongodb.get('users')
  if (!req.body.password || !req.body.value || !req.body.to || req.body.value <= 0) {
    return res.json({
      "status": "Failure",
      "code": 400,
      "message": "Missing parameters",
    })
  }
  users.findOne({email: req.body.email}).then((user) => {
    if (!user) {
      return res.json({
        "status": "Failure",
        "code": 401,
        "message": "Unauthorized",
      })
    }
    try {
      const keys = {pubkey: user.ethereumAddress, privkey: user.ethereumPrivKey}      
      rawTx = createRawTransaction('', keys, req.body.to, (new BigNumber(req.body.value).times(new BigNumber(10).pow(18))).toNumber(), req.body.password, req.body.gas);
    } catch (e) {
      console.log("error while sending ether", e);
      return res.status(403).json({
        "status": "Failure",
        "code": 403,
        "message": "Error in creating the transaction check parameters.",
      });
    }
    return sendTransaction(rawTx)
      .then((hash) => {
        return res.status(200).json({
          "status": "success",
          "code": 201,
          "message": "Transaction complete",
          "data": {
            "hash": hash,
          }
        });
      })
      .catch((e) => {
        console.log("error while sending tx ether",e);
        return res.status(500).json({
          "status": "Failure",
          "code": 500,
          "message": "Error when transferring Ether",
        });
      })
  })
}

module.exports = function (router) {

  router.get('/addresses/{email}',
    (req, res, next) => {
      next();
    },
    // requireAuth,
    getAllAddresses,
  );

  router.get('/addresses/contribute',
    (req, res, next) => {
      next();
    },
    // requireAuth,
    getContributionAddresses
  );

  router.post('/transfer/token',
  (req, res, next) => {
    next();
  },
  // requireAuth,
  transferNewToken
);

  router.post('/transfer/ether',
  (req, res, next) => {
    next();
  },
  // requireAuth,
  transferEther
  );

}